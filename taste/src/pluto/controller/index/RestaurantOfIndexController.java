package pluto.controller.index;

import com.jfinal.plugin.activerecord.Model;

import pluto.controller.base.BaseController;
import pluto.controller.dto.MyAppJSON;
import pluto.model.Master;
import pluto.model.News;
import pluto.model.Restaurant;
import pluto.model.Show;

public class RestaurantOfIndexController extends BaseController {
	public void index(){
	}
	
	public void initJSON(){
		MyAppJSON<Restaurant> appJSON=new MyAppJSON();
		appJSON.addItems(Restaurant.dao.findForIndex(10).getList());
		renderJson(appJSON);
	}
}
