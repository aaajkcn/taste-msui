package pluto.controller;

import pluto.controller.base.BaseController;
import pluto.controller.dto.MyAppJSON;
import pluto.model.Master;
import pluto.model.MasterMaterial;
import pluto.model.MasterMethod;
import pluto.model.MasterTip;

public class MasterController extends BaseController {
	public void index(){
		myRender("/master.html");
	}
	
	public void show(){
		Integer masterId = getParaToInt(0);
//		setAttr("one", Master.dao.findById(masterId));
//		setAttr("materials", MasterMaterial.dao.findByMasterId(masterId));
//		setAttr("methods", MasterMethod.dao.findByMasterId(masterId));
//		setAttr("tips", MasterTip.dao.findByMasterId(masterId));
//		myRender("/master/detail.html");
		MyAppJSON appJSON=new MyAppJSON();
		appJSON.addItem(Master.dao.findById(masterId));
		appJSON.addItemMap("meterials", MasterMaterial.dao.findByMasterId(masterId));
		appJSON.addItemMap("methods", MasterMethod.dao.findByMasterId(masterId));
		appJSON.addItemMap("tips", MasterTip.dao.findByMasterId(masterId));
		renderJson(appJSON);
	}
}
